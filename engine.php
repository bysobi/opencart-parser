<?php
include_once "Curl/Curl.php";

require 'config.php';

$engine = new engine;
$result = $engine->addModule($config);
return  $result;

class engine {
     public $url = 'https://www.opencart.com/index.php?route=account';
     public $cookieFile = 'C:/OpenServer/domains/parse.opencart/cookie.txt';
     public $userAgent = 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36 FirePHP/4Chrome';

    public function addModule($data) {
      // login
      $curl = new Curl();
      $curl->setUserAgent($this->userAgent);
      $curl->setCookieJar($this->cookieFile);      
      $curl->setCookieFile($this->cookieFile);
      $curl->post($this->url.'/login', array(
          'email'       => $data['login'],
          'password'    => $data['password'],
          'pin'         => $data['pin'],
          'redirect'    => ''
      ));
      if ($curl->error) {
            echo 'Error: ' . $curl->errorCode . ': ' . $curl->errorMessage . "\n";
            exit;
      }
      $info = $curl->getInfo();

      parse_str($info['redirect_url'], $value);
      $member_token = $value['member-token'];
      $redirect_url = $info['redirect_url'];

      // pin code
      $curl->setUserAgent($this->userAgent);
      $curl->setCookieFile($this->cookieFile);
      $curl->setCookieJar($this->cookieFile);
      $curl->post($this->url.'/security&member-token='.$member_token, array(
          'member-token'      => $member_token,
          'pin'               => $data['pin']
      ));

      // add module
      $curl->setUserAgent($this->userAgent);
      $curl->setCookieFile($this->cookieFile);
      $curl->setOpt(CURLOPT_FOLLOWLOCATION, true);
      $curl->post($this->url.'/extension/add&member-token='.$member_token, array(
          'name'                                            => $data['title'],
          'description'                                     => $data['description'],
          'extension_category_id'                           => $data['category_id'],
          'tag'                                             => $data['tag'],   
          'extension_download[0][name]'                     => 'asdasdasdasd',
          'extension_download[0][extension_download_id]'    => '5',
          'extension_download[0][filename]'                 => '5845425125118.zip',
          'extension_download[0][mask]'                     => 'asd.zip',
          'extension_download[0][download][]'               =>  '47',
          'extension_download[0][download][]'               =>  '46',
          'publish'                                         =>  '1'
      ));

      echo $curl->rawResponse; 
    }
  }
?>
