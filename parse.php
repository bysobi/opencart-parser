<?php
include_once('simple_html_dom.php');

ini_set('user_agent', 'My-Application/2.5');

function scraping_oc($category_id, $extension_id, $filter = '', $cache, $page = 1) {
	if(!empty($filter)){
		$filter = "&filter_search=".urlencode($filter).$cache;
	}

	$html = file_get_html('https://www.opencart.com/index.php?route=marketplace/extension'.$filter.'&filter_category_id='.$category_id.'&page='.$page);
	$modules = $html->find('div.extension-preview');

	$arrayLinks = array();
	foreach ($modules as $module) {
		$arrayLinks[] = $module->find('a', 0)->href;
	}

	foreach ($arrayLinks as $position => $link) {
		$findedId = strripos($link, $extension_id);

		if($findedId){
			echo json_encode(array(
				'status' 	=> 'success',
				'page' 		=> $page,
				'position' 	=> $position-1,
				'link'		=> $link
			));

			return true;
		}
	}

	echo json_encode(array('status' => 'error'));

	$html->clear();
	unset($html);
}

$category_id = $_POST['extension_category_id'];
$extension_id = $_POST['extension_id'];
$filter_search = $_POST['filter'] ? $_POST['filter'] : $_POST['tag_word'];
$cache = $_POST['cache'];

scraping_oc($category_id, $extension_id, $filter_search, $cache); 
?>